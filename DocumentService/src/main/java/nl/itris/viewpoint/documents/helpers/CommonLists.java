package nl.itris.viewpoint.documents.helpers;

import java.util.List;
import java.util.Vector;

public class CommonLists {

	public CommonLists () {
	}
	
	public List<CommonOriginalTable> getOriginalTables () {
		
		List<CommonOriginalTable> listCommonOriginalTable = new Vector<CommonOriginalTable>();
        
		listCommonOriginalTable.add(new CommonOriginalTable("DIS_BLOBS"));
		listCommonOriginalTable.add(new CommonOriginalTable("DIS_DOCUMENTS"));
		listCommonOriginalTable.add(new CommonOriginalTable("RED_BLOBS"));
		listCommonOriginalTable.add(new CommonOriginalTable("*OTHER*"));
		
        return listCommonOriginalTable;
        
	}

	
}
