package nl.itris.viewpoint.documents.repositories;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import nl.itris.viewpoint.documents.entities.EditableDocument;

public interface EditableDocumentRepository extends JpaRepository<EditableDocument, Long> {
	
	Optional<EditableDocument> findByDocumentnameDownload(String documentname_download);
	
}