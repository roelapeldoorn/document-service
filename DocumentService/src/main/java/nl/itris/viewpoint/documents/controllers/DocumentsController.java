package nl.itris.viewpoint.documents.controllers;

import java.io.IOException;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import nl.itris.viewpoint.documents.entities.DomainInformation;
import nl.itris.viewpoint.documents.entities.EditableDocument;
import nl.itris.viewpoint.documents.helpers.CommonLists;
import nl.itris.viewpoint.documents.helpers.DownloadNameGenerator;
import nl.itris.viewpoint.documents.helpers.ShowMode;
import nl.itris.viewpoint.documents.repositories.DomainInformationRepository;
import nl.itris.viewpoint.documents.repositories.EditableDocumentRepository;

@Controller
@RequestMapping("/documents")
public class DocumentsController {

    private final EditableDocumentRepository editableDocumentRepository;
    private final DomainInformationRepository domainInformationRepository;
    
    @Autowired
    public DocumentsController(EditableDocumentRepository editableDocumentRepository, DomainInformationRepository domainInformationRepository) {
        this.editableDocumentRepository = editableDocumentRepository;
        this.domainInformationRepository = domainInformationRepository;
    }
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showAll(Locale locale, Model model) {
        return getDocumentsIndex(model);
    }
    
    @GetMapping("/new")
    public String showNew(Model model) {
    	
    	EditableDocument editableDocument = new EditableDocument();
    	model.addAttribute("onedocument", editableDocument);
		
    	ShowMode showMode = new ShowMode();
    	showMode.setShowMode("new");
    	model.addAttribute("modus", showMode);
    	
		setCommonModels(model);
		
        return "documents-edit";
        
    }

    @GetMapping("/download/{documentid}")
    public ResponseEntity<byte[]> showDownload(@PathVariable("documentid") long documentid, Model model) {
    	EditableDocument editableDocument = editableDocumentRepository.findById(documentid)
          .orElseThrow(() -> new IllegalArgumentException("Document with ID: " + documentid + " not found!"));
    	
    		Long domainId = Long.valueOf(1);
    		Optional<DomainInformation> domainInformationOptional = domainInformationRepository.findById(domainId);
    		
    		DomainInformation domainInformation = new DomainInformation();
    		
    		if (domainInformationOptional.isPresent()) {
    			domainInformation = domainInformationOptional.get();
    		} else {
    			domainInformation.setDomainname("domain_not_found.nl");
    		}
    		
    		String documentNameDownload = DownloadNameGenerator.getAlphaNumericString(24, domainInformation.getDomainname());
    		
    		editableDocument.setDocumentnameDownload(documentNameDownload);
    		editableDocument.setDocumentAction("move_and_open");
    		editableDocumentRepository.save(editableDocument);
            
    		return ResponseEntity.ok()
	    	        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + documentNameDownload + "\"")
	    	        .body(editableDocument.getDocument());
    
    }
    
    @GetMapping("/download-withoutclient/{documentid}")
    public ResponseEntity<byte[]> showDownloadWithoutClient(@PathVariable("documentid") long documentid, Model model) {
    	EditableDocument editableDocument = editableDocumentRepository.findById(documentid)
          .orElseThrow(() -> new IllegalArgumentException("Document with ID: " + documentid + " not found!"));
    	
    		
    		editableDocument.setDocumentAction("download_only");
    		editableDocumentRepository.save(editableDocument);
            
    		return ResponseEntity.ok()
    				.header(HttpHeaders.CONTENT_TYPE, "application/octet-stream")
	    	        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + editableDocument.getDocumentname() + "\"")
	    	        .body(editableDocument.getDocument());
    
    }

    @GetMapping("/delete/{documentid}")
    public String showDelete(@PathVariable("documentid") long documentid, Model model) {
    	EditableDocument editableDocument = editableDocumentRepository.findById(documentid)
          .orElseThrow(() -> new IllegalArgumentException("Document met ID: " + documentid + " niet gevonden!"));

    	ShowMode showMode = new ShowMode();
    	showMode.setShowMode("delete");
    	showMode.setShowReadonly("delete");
    	showMode.setShowPosttext("Delete");
    	
    	model.addAttribute("onedocument", editableDocument);
		model.addAttribute("modus", showMode);

    	return "documents-edit";
    
    }

    @PostMapping("/postsave/{documentid}")
    public String saveDocument(@PathVariable("documentid") long id, 
    		@RequestParam("documentfile") MultipartFile file, 
    		@Valid EditableDocument editableDocument,
    		HttpServletRequest request, 
    		BindingResult result, 
    		Model model) {

    	if (result.hasErrors()) {
            return "documents-edit";
        }
    	
    	if (file != null) {
    		try {
    			editableDocument.setDocument(file.getBytes());
    			editableDocument.setDocumentname(file.getOriginalFilename());
    			editableDocument.setDocumenttype(file.getContentType());
    			editableDocument.setDocumentsize(file.getSize());
    		} catch(IOException ioe) {
    		}
    	}
    	
    	editableDocument.setId(id);
    	editableDocument.setDocumentAction("new");
		editableDocument.setDocumentupdated(false);

    	editableDocumentRepository.save(editableDocument);
        
        return getDocumentsIndex(model);
    
    }
    
    @PostMapping("/postdelete/{documentid}")
    public String deleteDocument(@PathVariable("documentid") long documentid, 
    		@Valid EditableDocument editableDocument,
    		BindingResult result,
    		Model model) {
   
    	EditableDocument ed = editableDocumentRepository.findById(documentid)
    	          .orElseThrow(() -> new IllegalArgumentException("Document met ID: " + documentid + " niet gevonden!"));
    	
    	if (result.hasErrors()) {
            return "documents-edit";
        }
    	
        editableDocumentRepository.delete(ed);
        
        return getDocumentsIndex(model);
    
    }
    
    private String getDocumentsIndex(Model model) {

    	model.addAttribute("alldocuments", editableDocumentRepository.findAll());
    	
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	model.addAttribute("authenticated", authentication);

        return "documents-index";
    	
    }
    
    private void setCommonModels(Model model) {

    	CommonLists commonLists = new CommonLists();
    	model.addAttribute("commonoriginaltables", commonLists.getOriginalTables());
		
    }

}
