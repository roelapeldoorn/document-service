package nl.itris.viewpoint.documents.helpers;

public class ShowMode {

    private String showMode;
    private String showReadonly;
    private String showPosttext;

    public ShowMode() {
    	this.showReadonly = "save";
    	this.showPosttext = "Save";
    }
    
    public String getShowMode() {
		return showMode;
	}

	public void setShowMode(String showMode) {
		this.showMode = showMode;
	}

	public String getShowReadonly() {
		return showReadonly;
	}

	public void setShowReadonly(String showReadonly) {
		this.showReadonly = showReadonly;
	}

	public String getShowPosttext() {
		return showPosttext;
	}

	public void setShowPosttext(String showPosttext) {
		this.showPosttext = showPosttext;
	}
	
}
