package nl.itris.viewpoint.documents.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DSS_ROLE")
public class Role {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "role_Sequence")
    @SequenceGenerator(name = "role_Sequence", sequenceName = "DSS_ROLE_SEQ")
    private Long id;

    @Column(name = "NAME", nullable = false, length = 40)  
	private String name;

    public Role() {
    }

    public Role(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
    
}
