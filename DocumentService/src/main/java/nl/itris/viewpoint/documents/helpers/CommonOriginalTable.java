package nl.itris.viewpoint.documents.helpers;

public class CommonOriginalTable {
	
    private String table;

    public CommonOriginalTable () {
    }

    public CommonOriginalTable (String table) {
		this.table = table;
    }
	public String getTable() {
		return table;
	}
    
}
