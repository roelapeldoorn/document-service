package nl.itris.viewpoint.documents.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import nl.itris.viewpoint.documents.entities.DomainInformation;

public interface DomainInformationRepository extends JpaRepository<DomainInformation, Long> {
	
	List<DomainInformation> findAllById(long domainid);

}