package nl.itris.viewpoint.documents.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.itris.viewpoint.documents.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	Optional<User> findByEmailaddress(String emailaddress);

	Optional<User> findByResetToken(String resetToken);

}
