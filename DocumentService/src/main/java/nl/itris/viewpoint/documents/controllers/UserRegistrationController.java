package nl.itris.viewpoint.documents.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import nl.itris.viewpoint.documents.controllers.dto.UserRegistrationDto;
import nl.itris.viewpoint.documents.entities.User;
import nl.itris.viewpoint.documents.services.UserService;

@Controller
@RequestMapping("/registration")
public class UserRegistrationController {

    @Autowired
    private UserService userService;

    @ModelAttribute("user")
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return "registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") @Valid UserRegistrationDto userDto, BindingResult result){

        Optional<User> optionalUser = userService.findByEmailaddress(userDto.getEmailaddress());
        if (optionalUser.isPresent()){
            result.rejectValue("emailaddress", null, "There is already an account registered with that email");
        }

        if (result.hasErrors()){
            return "registration";
        }
        
        userService.save(userDto);
        
        return "redirect:/registration?success";
       
    }

}
