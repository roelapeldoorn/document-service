package nl.itris.viewpoint.documents.helpers;

public class DownloadNameGenerator {

	public static String getAlphaNumericString(int n, String domainName) {
  
		return (domainName + "xxxxx" + getUniqueKey(n) + "xxxxx.workfile").
				replaceAll(" ", "_space_").
				replaceAll(":", "_dubbelepunt_").
				replaceAll("/", "_slash_");
		   
    }
	
	private static String getUniqueKey(int n) {
		
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvyz";
        StringBuilder sb = new StringBuilder(n);
        
        for (int i = 0; i < n; i++) {
            int index = (int)(AlphaNumericString.length() * Math.random());
            sb.append(AlphaNumericString.charAt(index));
        }
  
		return sb.toString();
		
	}
}
