package nl.itris.viewpoint.documents.services;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetailsService;

import nl.itris.viewpoint.documents.controllers.dto.UserRegistrationDto;
import nl.itris.viewpoint.documents.entities.User;

public interface UserService extends UserDetailsService {

	public Optional<User> findByEmailaddress(String emailaddress);

    public User save(UserRegistrationDto registration);
    
    public User save(User user);
    
    public Optional<User> findUserByResetToken(String resetToken);
       
}
