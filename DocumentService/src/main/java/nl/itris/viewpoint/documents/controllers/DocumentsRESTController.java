package nl.itris.viewpoint.documents.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import nl.itris.viewpoint.documents.entities.EditableDocument;
import nl.itris.viewpoint.documents.repositories.EditableDocumentRepository;

@RestController
@RequestMapping("/REST/documents")
public class DocumentsRESTController {

	@Autowired
	EditableDocumentRepository editableDocumentRepository;

	@GetMapping("/getall")
	public ResponseEntity<List<EditableDocument>> getAllDocuments() {
		try {
			List<EditableDocument> editableDocuments = new ArrayList<EditableDocument>();

			editableDocumentRepository.findAll().forEach(editableDocuments::add);
			if (editableDocuments.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(editableDocuments, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/getone/{documentid}")
	public ResponseEntity<EditableDocument> getDocumentById(@PathVariable("documentid") long documentid) {
		Optional<EditableDocument> editableDocument = editableDocumentRepository.findById(documentid);

		if (editableDocument.isPresent()) {
			return new ResponseEntity<>(editableDocument.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/check/{documentname}")
	public ResponseEntity<EditableDocument> getDocumentByDocumentnameDownload(
			@PathVariable("documentname") String documentname) {

		Optional<EditableDocument> editableDocument = editableDocumentRepository
				.findByDocumentnameDownload(documentname);

		if (editableDocument.isPresent()) {
			EditableDocument editableDocumentDownload = editableDocument.get();
			editableDocumentDownload.setDocument(null);
			return new ResponseEntity<>(editableDocumentDownload, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/update/{documentname}")
	public ResponseEntity<EditableDocument> updateDocumentByDocumentnameDownload(
			@PathVariable("documentname") String documentname) {

		Optional<EditableDocument> editableDocument = editableDocumentRepository
				.findByDocumentnameDownload(documentname);

		if (editableDocument.isPresent()) {
			EditableDocument editableDocumentUpdate = editableDocument.get();
			editableDocumentUpdate.setDocumentAction("editing");
			editableDocumentRepository.save(editableDocumentUpdate);
			return new ResponseEntity<>(editableDocumentUpdate, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public @ResponseBody String uploadDocument(
    		@RequestParam("document") MultipartFile documentchanged, 
    		@RequestParam("documentid") String documentid) 
    				throws IOException {
    	
		String returnValue = "Document ID: " + documentid;
		
		Optional<EditableDocument> optionalEditableDocument = editableDocumentRepository
				.findById(Long.valueOf(documentid));

		if (optionalEditableDocument.isPresent()) {

			EditableDocument editableDocument = optionalEditableDocument.get();
			editableDocument.setDocument(documentchanged.getBytes());
			editableDocument.setDocumentname(documentchanged.getOriginalFilename());
			editableDocument.setDocumenttype(documentchanged.getContentType());
			editableDocument.setDocumentsize(documentchanged.getSize());
			editableDocument.setDocumentAction("uploaded");
			editableDocument.setDocumentupdated(true);

			editableDocumentRepository.save(editableDocument);
			
			returnValue = returnValue + " has been saved";
			
		} else {

			returnValue = returnValue + " not found in the database";
		
		}

		return returnValue;
    
    }

    @RequestMapping(value = "/uploaddocument", method = RequestMethod.POST)
    public @ResponseBody String uploadChangedDocument(@RequestParam("document") MultipartFile documentchanged, @RequestParam("editabledocument") EditableDocument editableDocument) throws IOException {
    	
		String returnValue = "EditableDocument ID: " + editableDocument.getId();
    	
		Optional<EditableDocument> optionalEditableDocument = editableDocumentRepository
				.findById(editableDocument.getId());

		if (optionalEditableDocument.isPresent()) {

			EditableDocument editableDocumentFound = optionalEditableDocument.get();
			editableDocumentFound.setDocument(documentchanged.getBytes());
			editableDocumentFound.setDocumentname(documentchanged.getOriginalFilename());
			editableDocumentFound.setDocumenttype(documentchanged.getContentType());
			editableDocumentFound.setDocumentsize(documentchanged.getSize());
			editableDocumentFound.setDocumentAction("uploaded");
			editableDocumentFound.setDocumentupdated(true);

			editableDocumentRepository.save(editableDocumentFound);
			
			returnValue = returnValue + " has been saved";
			
		} else {

			returnValue = returnValue + " not found in the database";
		
		}

		return returnValue;
    
    }

}