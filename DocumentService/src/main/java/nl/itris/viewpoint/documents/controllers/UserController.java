package nl.itris.viewpoint.documents.controllers;

import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import nl.itris.viewpoint.documents.controllers.dto.UserRegistrationDto;
import nl.itris.viewpoint.documents.entities.User;
import nl.itris.viewpoint.documents.helpers.ShowMode;
import nl.itris.viewpoint.documents.repositories.UserRepository;
import nl.itris.viewpoint.documents.services.UserService;

@Controller
@RequestMapping("/users")
public class UserController {

	private final UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Locale locale, Model model) {
    	
    	model.addAttribute("allusers", userRepository.findAll());
 
    	setModelsIndex(model);
    	
    	return "user-index";
    
    }
    
    @GetMapping("/new-user")
    public String showNewUser(Model model) {
    	User user = new User();
    	user.setId(Long.valueOf(0));

    	ShowMode showMode = new ShowMode();
    	showMode.setShowMode("new");

    	model.addAttribute("oneuser", user);
		model.addAttribute("modus", showMode);

    	setModelsEdit(model);
		
        return "user-edit";
        
    }

    @GetMapping("/change-user/{id}")
    public String showChangeUser(@PathVariable("id") long id, Model model) {
    	User user = userRepository.findById(id)
            .orElseThrow(() -> new IllegalArgumentException("User met ID: " + id + " niet gevonden!"));
    	
    	ShowMode showMode = new ShowMode();
    	showMode.setShowMode("edit");
    	
    	model.addAttribute("oneuser", user);
		model.addAttribute("modus", showMode);

    	setModelsEdit(model);
		
    	return "user-edit";
    
    }

    @GetMapping("/delete-user/{id}")
    public String showDeleteUser(@PathVariable("id") long id, Model model) {
    	User user = userRepository.findById(id)
            .orElseThrow(() -> new IllegalArgumentException("User met ID: " + id + " niet gevonden!"));
    	
    	ShowMode showMode = new ShowMode();
    	showMode.setShowMode("delete");
    	showMode.setShowReadonly("delete");
    	showMode.setShowPosttext("Delete");
    	
    	model.addAttribute("oneuser", user);
		model.addAttribute("modus", showMode);

    	setModelsEdit(model);
		
    	return "user-edit";
    
    }

    @PostMapping("/save/{id}")
    public String saveUser(@PathVariable("id") long id, @Valid User oneuser, BindingResult result, Model model) {

    	if (result.hasErrors()) {
            return "user-edit";
        }

        if (id == 0) {
        	UserRegistrationDto userRegistrationDto = new UserRegistrationDto();
        	userRegistrationDto.setFirstName(oneuser.getFirstName());
        	userRegistrationDto.setLastName(oneuser.getLastName());
        	userRegistrationDto.setEmailaddress(oneuser.getEmailaddress());
        	userRegistrationDto.setConfirmEmailaddress(oneuser.getEmailaddress());
        	userRegistrationDto.setPassword("Testen01");
        	userRegistrationDto.setConfirmPassword("Testen01");
        	
        	userService.save(userRegistrationDto);

        } else {
        	userRepository.save(oneuser);
        }
    	
        model.addAttribute("allusers", userRepository.findAll());
    	
    	setModelsIndex(model);
		
    	return "user-index";
    
    }
    
    @PostMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
    	User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("User met ID: " + id + " niet gevonden!"));

    	userRepository.delete(user);

    	model.addAttribute("allusers", userRepository.findAll());
    	
    	setModelsIndex(model);
		
        return "user-index";
        
    }       

    private void setModelsIndex(Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	model.addAttribute("authenticated", authentication);

    }

    private void setModelsEdit(Model model) {

    	setModelsIndex(model);

    }
    
}