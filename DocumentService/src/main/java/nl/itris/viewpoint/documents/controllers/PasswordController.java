package nl.itris.viewpoint.documents.controllers;

import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import nl.itris.viewpoint.documents.entities.User;
import nl.itris.viewpoint.documents.properties.MailProperties;
import nl.itris.viewpoint.documents.services.EmailService;
import nl.itris.viewpoint.documents.services.UserService;

@Controller
public class PasswordController {

	@Autowired
	private UserService userService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	MailProperties mailProperties;

	@RequestMapping(value = "/forgot", method = RequestMethod.GET)
	public String showForgotPasswordForm(Locale locale, Model model) {

		User user = new User();
		model.addAttribute("user", user);

		String message = "";
		model.addAttribute("message", message);

		return "forgot-my-password";

	}

	@RequestMapping(value = "/forgot/send", method = RequestMethod.POST)
	public String sendRequestPassword(ModelAndView modelAndView, @Valid User passwordResetUser,
			HttpServletRequest request, BindingResult result, Model model) {

		Optional<User> optionalUser = userService.findByEmailaddress(passwordResetUser.getEmailaddress());

		if (!optionalUser.isPresent()) {

			model.addAttribute("user", passwordResetUser);

			String message = "This emailaddress is not available. Ask your administrator.";
			model.addAttribute("message", message);

			return "forgot-my-password";

		} else {

			User user = optionalUser.get();
			user.setResetToken(UUID.randomUUID().toString());

			userService.save(user);

			String appUrl = request.getScheme() + "://" + request.getServerName();

			// Email message (from wordt gehaald uit de mail-properties)
			SimpleMailMessage passwordResetEmail = new SimpleMailMessage();
			passwordResetEmail.setFrom(getMailUsername());
			passwordResetEmail.setTo(user.getEmailaddress());
			passwordResetEmail.setSubject("Password Reset Request");
			passwordResetEmail.setText("To reset your password, click the link below:\n" + appUrl
					+ "/editdocuments/forgot/reset?token=" + user.getResetToken());

			emailService.sendEmail(passwordResetEmail);

		}

		model.addAttribute("redirectuser", passwordResetUser);

		return "redirect-my-password";

	}

	// Display form to reset password
	@RequestMapping(value = "/forgot/reset", method = RequestMethod.GET)
	public String displayResetPasswordPage(ModelAndView modelAndView, @RequestParam("token") String token,
			Model model) {

		Optional<User> optionalUser = userService.findUserByResetToken(token);

		if (optionalUser.isPresent()) {

			User user = optionalUser.get();
			model.addAttribute("resetuser", user);

			return "reset-my-password";

		} else { // Token not found in DB
			modelAndView.addObject("errorMessage", "Oops!  This is an invalid password reset link.");
		}

		return "login";

	}

	@PostMapping("/forgot/save")
	public String saveResetPassword(@Valid User resetUser, BindingResult result, Model model) {

		Optional<User> optionalUser = userService.findByEmailaddress(resetUser.getEmailaddress());

		if (!optionalUser.isPresent()) {
			return "login";
		} else {

			User user = optionalUser.get();

			user.setResetToken(null);
			user.setPassword(bCryptPasswordEncoder.encode(resetUser.getPassword()));

			// Save token to database
			userService.save(user);

		}

		return "login";

	}

	@GetMapping("/username")
	public String getMailUsername() {
		return mailProperties.getUsername();
	}

	// Going to reset page without a token redirects to login page
	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ModelAndView handleMissingParams(MissingServletRequestParameterException ex) {
		return new ModelAndView("redirect:login");
	}
}