package nl.itris.viewpoint.documents.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DSS_EDITABLE_DOCUMENTS")
@NamedQueries({
    @NamedQuery(
    		name = "EditableDocument.findByDocumentnameDownload", 
    		query="select ed from EditableDocument ed where ed.documentnameDownload = :documentname_download")
})
public class EditableDocument {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "editabledocuments_Sequence")
    @SequenceGenerator(name = "editabledocuments_Sequence", sequenceName = "DSS_EDITABLE_DOCUMENTS_SEQ")
	private long id;

	@Lob()
	@Column(name = "DOCUMENT")
	private byte[] document;

	@Column(name = "DOCUMENTNAME", nullable = false, length = 256)  
	private String documentname;

	@Column(name = "DOCUMENTTYPE", nullable = false, length = 128)  
	private String documenttype;

	@Column(name = "DOCUMENTSIZE", nullable = false)  
	private long documentsize;

	@Column(name = "DOCUMENTNAME_DOWNLOAD", length = 384)  
	private String documentnameDownload;

	@Column(name = "DOCUMENT_ACTION", length = 16)  
	private String documentAction;

	@Column(name = "DOCUMENT_ORIGINAL_TABLE", length = 32)  
	private String documentOriginalTable;

	@Column(name = "DOCUMENT_ORIGINAL_ID")  
	private long documentOriginalID;

	@Column(name = "DOCUMENTUPDATED")
	private boolean documentupdated;
	
	public EditableDocument() {

	}

	public EditableDocument(byte[] document, String documentname, String documenttype, long documentsize, String documentnameDownload, String documentOriginalTable, Long documentOriginalID) {
		this.document = document;
		this.documentname = documentname;
		this.documenttype = documenttype;
		this.documentsize = documentsize;
		this.documentupdated = false;
		this.documentnameDownload = documentnameDownload;
		this.documentOriginalTable = documentOriginalTable;
		this.documentOriginalID = documentOriginalID;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public boolean isDocumentupdated() {
		return documentupdated;
	}
	public void setDocumentupdated(boolean documentupdated) {
		this.documentupdated = documentupdated;
	}
	public byte[] getDocument() {
		return document;
	}
	public void setDocument(byte[] document) {
		this.document = document;
	}
	public String getDocumentname() {
		return documentname;
	}
	public void setDocumentname(String documentname) {
		this.documentname = documentname;
	}
	public String getDocumenttype() {
		return documenttype;
	}
	public void setDocumenttype(String documenttype) {
		this.documenttype = documenttype;
	}
	public long getDocumentsize() {
		return documentsize;
	}
	public void setDocumentsize(long documentsize) {
		this.documentsize = documentsize;
	}
	public String getDocumentnameDownload() {
		return documentnameDownload;
	}
	public void setDocumentnameDownload(String documentnameDownload) {
		this.documentnameDownload = documentnameDownload;
	}
	public String getDocumentOriginalTable() {
		return documentOriginalTable;
	}
	public void setDocumentOriginalTable(String documentOriginalTable) {
		this.documentOriginalTable = documentOriginalTable;
	}
	public long getDocumentOriginalID() {
		return documentOriginalID;
	}
	public void setDocumentOriginalID(long documentOriginalID) {
		this.documentOriginalID = documentOriginalID;
	}
	public String getDocumentAction() {
		return documentAction;
	}
	public void setDocumentAction(String documentAction) {
		this.documentAction = documentAction;
	}

	@Override
	public String toString() {
		return "EditableDocument [id=" + id + 
								", documentname=" + documentname + 
								", documenttype=" + documenttype + 
								", documentsize=" + documentsize + 
								", documentupdated=" + documentupdated + 
								", documentOriginalTable=" + documentOriginalTable + 
								", documentOriginalID=" + documentOriginalID + 
								"]";
	}

}
