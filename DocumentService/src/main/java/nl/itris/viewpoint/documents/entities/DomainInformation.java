package nl.itris.viewpoint.documents.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "DSS_DOMAIN_INFORMATION")
public class DomainInformation {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "domaininformation_Sequence")
	@SequenceGenerator(name = "domaininformation_Sequence", sequenceName = "DSS_DOMAIN_INFORMATION_SEQ")
	private long id;

	@Column(name = "DOMAINNAME", nullable = false, length = 256)  
	private String domainname;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDomainname() {
		return domainname;
	}
	public void setDomainname(String domainname) {
		this.domainname = domainname;
	}
	
}
