package nl.itris.viewpoint.documents.controllers;

import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import nl.itris.viewpoint.documents.entities.DomainInformation;
import nl.itris.viewpoint.documents.helpers.ShowMode;
import nl.itris.viewpoint.documents.repositories.DomainInformationRepository;

@Controller
@RequestMapping("/domain")
public class DomainInformationController {
	
	private final DomainInformationRepository domainInformationRepository;
    
    @Autowired
    public DomainInformationController(DomainInformationRepository domainInformationRepository) {
        this.domainInformationRepository = domainInformationRepository;
    }
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showAll(Locale locale, Model model) {
        return getDomainIndex(model);
    }

    @GetMapping("/edit/{domainid}")
    public String showEditDomain(@PathVariable("domainid") long domainid, Model model) {
    	
    	DomainInformation domainInformation = new DomainInformation();
    	Optional<DomainInformation> optionalDomainInformation =  domainInformationRepository.findById(domainid);
    	if (optionalDomainInformation.isPresent()){
    		domainInformation = optionalDomainInformation.get();
    	} else {
        	domainInformation.setId(1);
        	domainInformation.setDomainname("http://<hostname_to_use>:8080/editdocuments/");
            domainInformationRepository.save(domainInformation);
    	}
    	
    	ShowMode showMode = new ShowMode();
    	showMode.setShowMode("edit");
    	showMode.setShowReadonly("save");
    	showMode.setShowPosttext("Save");
    	
    	model.addAttribute("onedomain", domainInformation);
		model.addAttribute("modus", showMode);

    	return "domain-edit";
    
    }

    @PostMapping("/postsave/{domainid}")
    public String saveDomain(@PathVariable("domainid") long id, 
    		@Valid DomainInformation domainInformation,
    		HttpServletRequest request, 
    		BindingResult result, 
    		Model model) {

    	if (result.hasErrors()) {
            return "domain-edit";
        }
    	
    	domainInformation.setId(id);
        domainInformationRepository.save(domainInformation);
        
        return getDomainIndex(model);
    
    }
    
    private String getDomainIndex(Model model) {

    	if (domainInformationRepository.findAll().size() == 0) {
    		DomainInformation domainInformation = new DomainInformation();
        	domainInformation.setId(1);
            domainInformation.setDomainname("http://<hostname_to_use>:8080/editdocuments/");
            domainInformationRepository.save(domainInformation);
    	}
    	
    	model.addAttribute("alldomains", domainInformationRepository.findAll());
    	
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    	model.addAttribute("authenticated", authentication);

        return "domain-index";
    	
    }


}
