package nl.itris.viewpoint.documents.entities;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "DSS_USER", uniqueConstraints = @UniqueConstraint(columnNames = "EMAILADDRESS"))
@NamedQueries({
    @NamedQuery(
    		name = "User.findByEmailaddress", 
    		query="select u from User u where u.emailaddress = :emailaddress")
})

public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "user_Sequence")
    @SequenceGenerator(name = "user_Sequence", sequenceName = "DSS_USER_SEQ")
	private long id;

    @Column(name = "FIRST_NAME", nullable = false, length = 40)  
    private String firstName;
    
    @Column(name = "LAST_NAME", nullable = false, length = 48)  
    private String lastName;
    
    @Column(name = "EMAILADDRESS", nullable = false, length = 128)  
    private String emailaddress;
    
	@Column(name = "PASSWORD", nullable = false, length = 128)  
    private String password;

    @Transient
    private String confirmPassword;
    
	@Column(name = "RESET_TOKEN", nullable = true, length = 256)  
	private String resetToken;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "DSS_USER_ROLE",
            joinColumns = @JoinColumn(
                    name = "USER_ID", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "ROLE_ID", referencedColumnName = "id"))
    private Collection<Role> roles;

    public User() {
    }

    public User(String firstName, String lastName, String emailaddress, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailaddress = emailaddress;
        this.password = password;
    }

    public User(String firstName, String lastName, String emailaddress, String password, Collection<Role> roles) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailaddress = emailaddress;
        this.password = password;
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return firstName + " " + lastName;
    }

    public String getEmailaddress() {
        return emailaddress;
    }

    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getConfirmPassword() {
    	if (this.confirmPassword == null) {
    		return this.password;
    	}
        return this.confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public Collection<Role> getRoles() {
        return roles;
    }

    public void setRoles(Collection<Role> roles) {
        this.roles = roles;
    }

	public String getResetToken() {
		return resetToken;
	}

	public void setResetToken(String resetToken) {
		this.resetToken = resetToken;
	}

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", emailaddress='" + emailaddress + '\'' +
                ", password='" + "*********" + '\'' +
                ", roles=" + roles +
                '}';
    }
    
}
